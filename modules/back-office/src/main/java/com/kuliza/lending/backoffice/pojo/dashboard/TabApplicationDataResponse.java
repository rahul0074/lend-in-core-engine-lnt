package com.kuliza.lending.backoffice.pojo.dashboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kuliza.lending.backoffice.models.Cards;
import com.kuliza.lending.backoffice.models.Tabs;
import com.kuliza.lending.backoffice.models.TabsVariablesMapping;
import com.kuliza.lending.backoffice.pojo.dashboard.config.CardsConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.config.VariablesConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.config.VariablesOptionListConfig;
import com.kuliza.lending.backoffice.utils.BackOfficeConstants;

public class TabApplicationDataResponse {

	List<CardsConfig> cards;
	List<VariablesConfig> variables;

	public TabApplicationDataResponse() {
		super();
	}

	public TabApplicationDataResponse(List<CardsConfig> cards, List<VariablesConfig> variables) {
		super();
		this.cards = cards;
		this.variables = variables;
	}

	public TabApplicationDataResponse(Tabs tab, Map<String, Object> data) {
		super();
		this.cards = new ArrayList<>();
		this.variables = new ArrayList<>();
		Set<Cards> tabCards = tab.getCards();
		for (Cards card : tabCards) {
			this.cards.add(new CardsConfig(card, data));
		}
		Set<TabsVariablesMapping> tvmappings = tab.getTabVariables();
		for (TabsVariablesMapping tvmap : tvmappings) {
			if (tvmap.getVariable().getType().equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN)) {
				this.variables.add(
						new VariablesOptionListConfig(tvmap.getVariable(), data.get(tvmap.getVariable().getMapping())));
			} else {
				this.variables
						.add(new VariablesConfig(tvmap.getVariable(), data.get(tvmap.getVariable().getMapping())));
			}
		}

	}

	public List<CardsConfig> getCards() {
		return cards;
	}

	public void setCards(List<CardsConfig> cards) {
		this.cards = cards;
	}

	public List<VariablesConfig> getVariables() {
		return variables;
	}

	public void setVariables(List<VariablesConfig> variables) {
		this.variables = variables;
	}

}
