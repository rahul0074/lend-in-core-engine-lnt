package com.kuliza.lending.backoffice.utils;

import javax.servlet.http.HttpServletRequest;

import com.kuliza.lending.backoffice.exceptions.RoleNotAvailableException;

public class BackOfficeUtils {

	public static String checkRoleInRequest(HttpServletRequest request) {
		String roleName = "";
		if (request.getAttribute("userRole") != null) {
			roleName = request.getAttribute("userRole").toString();
			if (roleName.startsWith("ROLE_")) {
				roleName = roleName.substring(5);
			}
		} else {
			throw new RoleNotAvailableException("Role not available in request object");
		}
		return roleName;
	}

	/**
	 * This is an utility method used to check if a given string is not null and
	 * not empty
	 * 
	 * @param value
	 * @return true if String is not empty else false
	 */
	public static boolean isNotEmpty(String value) {
		return (value != null) && (!value.trim().equals(""));
	}

}
