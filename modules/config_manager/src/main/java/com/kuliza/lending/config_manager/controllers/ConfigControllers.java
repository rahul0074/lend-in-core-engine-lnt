package com.kuliza.lending.config_manager.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.config_manager.pojo.ConfigInput;
import com.kuliza.lending.config_manager.services.ConfigServices;

@RestController
@RequestMapping("/api/config/")
public class ConfigControllers {

	@Autowired
	private ConfigServices configServices;

	private static final Logger logger = LoggerFactory.getLogger(ConfigControllers.class);

	@RequestMapping(method = RequestMethod.GET, value = "get")
	public ResponseEntity<Object> getConfig(@RequestParam(required = true) String clientId) {
		try {
			return CommonHelperFunctions.buildResponseEntity(configServices.getConfigs(clientId));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = "set")
	public ResponseEntity<Object> setConfig(@Valid @RequestBody ConfigInput input) {
		try {
			return CommonHelperFunctions.buildResponseEntity(configServices.setConfig(input));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

}
